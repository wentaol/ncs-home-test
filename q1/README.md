# Dependencies
numpy, pandas

# Usage
```python cleandata.py inputdir output.csv```
Where ```inputdir``` is the directory with input csv files

This script processes all csvs in inputdir, and outputs one processed csv including headers
- delete the rows which do not contain a name
- split the name column  into first and last columns.
- ensure the price column contains only numbers
- add a new column, which will be true if the price is greater than 100, and false otherwise.