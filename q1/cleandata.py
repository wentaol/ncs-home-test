import sys
import os
import glob
import pandas as pd
import numpy as np
import re

if len(sys.argv) != 3:
    print("Usage: python cleandata.py inputdir outputfilename")
intputdir = sys.argv[1]
outfile = sys.argv[2]

filenames = glob.glob(os.path.join(intputdir, "*.csv"))
#print(filenames)
df = pd.concat([pd.read_csv(fn, dtype=str) for fn in filenames],
               ignore_index=True)
# coerce to numeric column and remove rows with NaNs
df['price_num'] = pd.to_numeric(df['price'], errors='coerce')
df = df[df["name"].notnull() & df['price_num'].notnull()]
# Remove honorifics and suffixes from name
pattern = re.compile("(^(Mr|Ms|Dr|Miss|Mrs)\.?\s+)|(\s+(MD|DDS|PhD|DVM|Jr\.|I|II|III|IV|V)$)")
df["name"] = df["name"].apply(lambda s : pattern.sub("", s))
df.reset_index(drop=True, inplace=True)
# Split name and add >100 column
df[["first", "last"]] = df["name"].str.split(" ", 1, expand=True)
df[">100"] = df["price_num"] > 100

outdf = df[["first", "last", "price", ">100"]]
outdf.to_csv(outfile, index=False)