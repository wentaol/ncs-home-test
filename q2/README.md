# Dependencies
numpy, pandas, sklearn, seaborn

# Data preparation
Create a "data" directory and download car.data from the following link:
https://archive.ics.uci.edu/ml/datasets/Car+Evaluation

# Description
Run jupyter notebook to train the model and obtain prediction result.
The trained model can later be saved and loaded elsewhere via pickle.